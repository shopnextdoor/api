# ShopNextDoor API

## Inspiration
> Durch Corona liegt der lokale Handel brach und viele Familien sorgen sich um ihre Existenzen. Wir möchten gerade Non-Digital-Natives eine einfache Möglichkeit bieten mit ihren bestehenden und bestenfalls neuen Kunden in Kontakt zu treten und weiterhin der lokalen Nachfrage nachzukommen.
>
> Quelle: https://devpost.com/software/shopnextdoor 

## Installation

### Vorrausetzungen
 - PHP Version 7.3
 - composer
 - Postgresql-Server
 - postgis-Plugin für den Postgresql-Server
  
### PHP installieren

Für Debian / Ubuntu:
```
apt install php7.3 php7.3-intl php7.3-mysql php7.3-json php7.3-common php7.3-pgsql
```

### Postgis plugin für Postgres installieren (linux, debian , mint)

**als user root**
für postgres in Version 9.5
```
curl https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add -
sh -c 'echo "deb http://apt.postgresql.org/pub/repos/apt/ $(lsb_release -sc) main" >> /etc/apt/sources.list.d/postgresql.list'
aptitude update
aptitude install postgresql-9.5-postgis-3
```

**als user postgres**
```
psql
```
_templates auf UTF8 und postgis ändern:_
```
# update pg_database set datallowconn = TRUE where datname = 'template0';
UPDATE 1
# update pg_database set datallowconn = TRUE where datname = 'template1';
UPDATE 1
# \c template0
You are now connected to database "template0" as user "postgres".
# CREATE EXTENSION postgis;
CREATE EXTENSION
# update pg_database set datistemplate = FALSE where datname = 'template1';
UPDATE 1
# drop database template1;
DROP DATABASE
# create database template1 with template = template0 encoding = 'UTF8';
CREATE DATABASE
# update pg_database set datistemplate = TRUE where datname = 'template1';
UPDATE 1
# \c template1
You are now connected to database "template1" as user "postgres".
# update pg_database set datistemplate = FALSE where datname = 'template0';
UPDATE 1
# drop database template0;
DROP DATABASE
# create database template0 with template = template1 encoding = 'UTF8';
CREATE DATABASE
# update pg_database set datistemplate = TRUE where datname = 'template0';
UPDATE 1
# update pg_database set datallowconn = FALSE where datname = 'template0';
UPDATE 1
# update pg_database set datallowconn = FALSE where datname = 'template1';
UPDATE 1
```
_zum prüfen ob PostGIS aktiv ist:_
```
SELECT postgis_full_version();
```
_um psql zu beenden:_
```
\q
```

### vHost
Das `DocumentRoot` ist das `public` Verzeichnis.

### Projekt konfigurieren

Die `.env`-Datei muss dupliziert und als `.env.local` abgespeichert werden. 
In `.env.local`-Datei muss nun die Datenbankverbindung hinterlegt werden.

### Datenbank erstellen

Die Datenbank kann mit dem Befehl `php bin/console doctrine:database:create` hinzugefügt werden.

Zum erstellen der Datenbankstruktur muss einmal der Befehl `php bin/console doctrine:schema:create` ausgeführt werden.

## Testen
### Test-Daten
Test-Daten können über symfony fixtures erstellt werden:

```
php bin/console doctrine:fixtures:load
```

### Swagger-UI

Die Swagger-API Dokumentation kann über `/api/doc` aufgerufen werden.

Mit der Oberfläche können die Daten sehr einfach geprüft und abgeruft werden.
