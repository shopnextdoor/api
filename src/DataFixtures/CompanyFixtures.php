<?php

namespace App\DataFixtures;

use App\Entity\Company;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

/**
 * CompanyFixtures
 * @package App\DataFixtures
 */
class CompanyFixtures extends Fixture
{
    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $company = new Company();

        $company->setValue(json_encode(
            [
                'companyName' => 'Dahlmann',
                'ownerName' => 'Dahlmann',
                'zip' => '42285',
                'city' => 'Wuppertal',
                'street' => 'Bendahler Straße 27',
                'contactTimes' => 'Mo. bis Sa. 08:00 Uhr bis 17:00 Uhr',
                'phone' => '+49 202 2802731',
                'email' => '',
                'facebook' => '',
                'web' => '',
                'description' => '',
                'images' => [
                    [
                        'type' => 'business',
                        'base64' => 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAKBueIx4ZKCMgoy0qqC+8P//8Nzc8P//////////////////////////////////////////////////////////2wBDAaq0tPDS8P//////////////////////////////////////////////////////////////////////////////wAARCAEsAZADASIAAhEBAxEB/8QAGAABAQEBAQAAAAAAAAAAAAAAAAECAwT/xAAmEAEBAAEEAgIDAQEBAQEAAAAAARECITFBUWEDEjJxgSJCkVLw/8QAFQEBAQAAAAAAAAAAAAAAAAAAAAH/xAAVEQEBAAAAAAAAAAAAAAAAAAAAAf/aAAwDAQACEQMRAD8A6gAAAAAAAAoIAAAAKgAAAAAAAAAAAAAAAKAAAAAAAAAAAAACAAAAAAAAAAAAAAAoAIACoAAAAAAAAAAAAMavkk2m9BscZ8mqRZ8lxNgdVYnyab21NUvcBQAAAAAAAAAAQAAAAAEAVFAAABQQAAABQBAABQEFQAAARQAZ1app5BpnVrmn9uerXbxtGQXVrur1EEyCmUAUwmTILMzitffVO6zkyDc12TDfx6rqly4Za0avrcg9AigCAKgAAAAAAgAACooAAAAAKCAAoAAAAIAAABbJyAlsk3rGr5P/AJ/9c7bQb1fJbxswhkFS1FABqQEkS7VtLAZDAAGVxQQi2bIDt8WrMx4befTcXL0S5mYAAAAAAACAAAAAAAoAAAAAAACooAAAIAM6vkk43crquoHTV8knDlbbyJkFTIAigAGN1kt42A08tE04UBFTMtBLpT63y2vQOeL2q2bw6AYvLbOrkEdPi1f83+OSy4B6RNOr7TKgAgCoAqAAAAAAAAqAKAAAAAAogKMatc0uerXdQOmrXJ7rnq13UymQVLTkAMDWmbAmEsbvKXmAy1JtvF0yYzDVwCdtaN5lM4qS2TE2BqpnwmPJkDGeabdJyAu/lc30mb4Pt6Bbb4TMMwzPIGYmrfFXaICIt4Ab+PVi46rs80d9Gr7afYKAAAAAAAAAAAAACgAAAAzq1zT+wXVqmmbuN16r2a9f2ZyCpkAAXTAMJhu8J2DM34jc4XTwAnZ2md9jGeUF03GnHZvfSZkM28RRcSJb4MeTZBMW8qoBhPZy10CFLxswo0YjO8alz+wTB+2uUwCXhMLzNgGW/j1YvpnVygPSOfx683F/joAAAAAAAAAAAACgAAAzqlsxLhx1SzmPQlksxQeZWsYTGwIuNiaVoJNLUmEn5T0tv9AqXs3ptAJbjEMeTOQD7Trc3pphbgCSNOc1VtBLcMctat6mLbiKLpalyk0zCyYAzuvSXlUGcZT6e2ycqOe85WbVu7RjnFBqUpgqCXbdD/o4uOlDV0y1eMJeQTLejVjV6YAekY+PVmY7jYAAAgAACgAAAKgCgAJquNNLqx7YurPYM3hKWm99Asxm1DacmfAGPJmGLeTAJvauIRm3IN9IaOGrwgnTF3rWLU/aiSW3ZblrTvC9AzJ/prTc6sE/JdE7Bai1ATG6peVQCchOVDVM5jOn8f61eWZzqBroJwIM3mVbwmraRKoc7dpV7L5BEsU1dAadWLl3lzHndPj1dA6AAAAAAoAAAAjOrXjoG6zbJzf4535LWbcg6XVL4ZYm7QLmTg3vok7T7bgv18luFrFBvTcwwmnOGrwgznbCYzFsZs3kUbmnC0KgdM38a1eGdX4/1RdE/wAl5izbTGbdwWfkum7MfZN6DpdUSXNZmny1ICgIM2VM1rMMAz9qZawYUSallySCBeKzY3eGeYohwSLef2CF/E9ICEuKvaA9Gm5mRz+LVi48uoIKAgqAo5TVLzVurTPYOl1Sc2Mavkk4c7qyyDWrXazkADCkm4LJupAC8M4vUWxdPOPAJc7nEytLzIDU4KFQSpz8i9s5xqyo2lWcJqmagl1RLcr9YuJAZktammGTIGDaM2m4NZJc1nFWSSg0ADNu6Z9NXnpP4Bkz7P4bKGVz+kxPJJvyg10xxcNxmzdQheInFxWuqDN3qYXsAS8rdqlBJtXpnEeau3x6s6ceAbAAQAcMVGsFBnCkAISbr0dAkanCdNdSAHYdAnZp4tOqku2IC3kz/rKb0mnyDcuWdWc7NREGcWrNJkyDSW7qzZvyBamV2/Zn9QExauJ3UznszFF26hn9RnNvaA1mftZzwzp5b2gKJ9p0n2vUQWzdi8tS3OK1hRzz7XN8tY9H1ngGc+l/iY2/rQLGdXLSagZu66b0nH6WcglIt5wgF4SeGumaBeF+PVjUnMTsHpRNNzplUAAHKJVkxE5oHRjeReyd0EvK3wTk5oC9pOasAu0ZuWqmQT6tbSM5NwaymUx5qyTPANMYtbvDFvpAxP2s54ZzfK6OVG2by0kQYtqN6uU+uaok54ax/qk078r3QZxyk03HDU7NNtoEmKXHZ/3VsyDOZ4M1rEVBmctCKLN6ZNPJeaCXfJ1sT8v4dAzjyXjHS4LMfoEhwY2SUGtU4qLfxZvkGmWksBnit6NMurdlrTcaoDtjAAIKA43ZIWnQLODoxxDUCTsnNpxpJ+ILOFgvQM3e8pifs3Z3BvKW49pp3yWbgZv6b0xj63LVz5Bq8MUxFoM/WtYsmyb5wtmQTE7q6TCyIJ2lty1OUnMUW/md0v5VJOQJxTRyT8b+jRyC/wDVD/qgKAgIooTk1ci6uQZ/6hKXmL3QRegiDM7jMm7XGqnaizixnhc2XhIAXgi8wGaHGyg7abnTlWPit3joCCoDz1eyeVnGQJzlLvcL0QC707wQndBf2l1RNs8HfALeEwtMcQDGE5q27k4tA03OT6mnhoGcbna1ASfk3GNLYFTC0QScpp/KLsZ0zdRP+qs7T7TwluQaxtTTpxc1n+FBrVtvLumfaTKybgZvlpPSgluD7LUsQPuv3Z+qYUb+08JndnBig2RnF8tRBnV+QurpKoXKXeKtBiNRDIJSF3AdPjuNX7dXCXFldgVABx6L1DsnIFpxDml5wBxDoq9gSHapOATtZzUnK7TsEvBfxLYfb0CziKzLbWgRNkxTALmTiNMzS1meQTUzu1RBjdcNYXAMzSuNlAZzthcHZlQwUvKAf2mJ4tXN8GfcAx6VnPtc30gomb4XPoBKufSZApm+DJt5BLczHaStbJjdQi3hjeVqXMBKjVQEo10zgFnDrpudMcY6fHdrAbABy6OjM9mb4A0xcbpnV6hi+QXsnKfUm3QLq4Z3XOauAZ3WaVwoMfVfquyoJwW/o/iZ/Siqz9vafb9oNH9Zz6M3wo1n2Z9M7+U/oN5T7e4zsA19vaZntP4f+AufRmpn2Z9g1qznZN/Jr5T+Af02ADZcRN1AxPJj2W+kzPALv5XfyzmGYDW6Zpn9n9Aze1lZl9qDV3IzmzleQTONqLjLM2oNRFS8gjWi/wCv2yvGAdgm8AcwxfJid0AzD/M7JZeATKyemdTX3k7tBcUx7Z+86h971AaxPa4nhz++pM6vIOv8Z1a8XDG97TALzeAQF/8ADPtkBoIUEz6XcAP6i5x0QAwABlAG9XLLWrpnigoAAAFT6+16AZAgLFOklBSgCC4i4kgJku56wWYAS5MmQUCA6fHc6f025/Hd7GwcMrJmJWpwFTvEX/U6TTuvQM3Od07W81JyCgdAbZ2CcALNkAGpoth9PbXaXj/96Bn6XyTR7b5uP2luJ+gY7wUtzaApj3BAaxPKXGdkyAAACKDWrpldfEZgKCgAAAgFIiwFRQEUAXVwXhM7LOgTldQW5BhTsvIBnCL0DWi41R0zPLjOWsg//9k='
                    ],
                    [
                        'type' => 'image1',
                        'base64' => 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAKBueIx4ZKCMgoy0qqC+8P//8Nzc8P//////////////////////////////////////////////////////////2wBDAaq0tPDS8P//////////////////////////////////////////////////////////////////////////////wAARCAEsAZADASIAAhEBAxEB/8QAGAABAQEBAQAAAAAAAAAAAAAAAQACAwT/xAAmEAEBAAMAAwADAAEFAQEAAAAAAQIRIRIxQQNRYSITMkJxkVKB/8QAFgEBAQEAAAAAAAAAAAAAAAAAAAEC/8QAFhEBAQEAAAAAAAAAAAAAAAAAABEB/9oADAMBAAIRAxEAPwAQSBCQJJAkiARSgRQApAkkCRWgSOkgERsCmdjYrex5MqAdllqIJjK9brGUMNZCLSJSUyx1xsBiY1qYtzRBzymmfKtZMVGj5DYQgoNCoUkipHcZqoY1azDQG1sIHRFIApKJJAkiARIMnS+kAiuIA6WxsU6TNo30G9jY6EDaEgBUjXjwVi+jhes2/Dh7Vl0uO1rRGVRWb7Fm17rWtAx4M2adYrCkcW5TcYzZ+lQ+evTU/I52aUqjpbtmiVr2kWshozGAxpadPFaVGNLTegDPivFowGfEWV1h1AcdLTrqLUBlIgCkCSSKkNrZQrbKBqjaAHYS9ILa2NbWlEjpaQMqp1NC99CDTUilHfLQtak618Zt0fgOWU7wS6at7xSfxUPnxSzJnKdM4Cs8a15b4ZZlGZAOlvi9De6gL0+K3wY3dAWM6rr4mzS0cZxOlxlYsKLbUssY0lHTy0ZnPrlLpqZY/YDryrUc/H/5q8ssfYOnitMzOVrcoJFAGb1q+mLwCts7SKdpLaCQtGwa4NspQoybGrQKamP7MkRWZNm4z7Tl64pJYIMdaXrrWMkhqjOx1eKm5UaOh6al6rpEc8pT8/rfv4z46WopPo3kpLafGiMW7u2pdxqYzTNs3FGprXWPyXvDnjfjMm/YDG6rr7nHOzSxy1QPVLqnLs4zYg1ymYyM7u9NwBu76s8llz6xeim5cE7RROVUdPHYuBxv6blRXG42Mu96zcdrSOW9NTP99Xhdq4a91UOsb6uh3FlboOk/JZ7bmcrjtA9As24zKxqfk/YAstRFLNqyqxxtQGtteFdJjMYrdlHPxrUwPTsVTGRXi7fRmIgk2fE6QDS+FnL0Al63pzmN9tyqatM3H9NUa1NhjONb3tmC/wAZV0FjOE+1uqgnpezPSBmxmxtWbBnHLmqJPrXjIOaCK4+U4x4abls+C5bugEulL5X9DL+LHHd99UbuJkHwbsQo/JNVTQzvVJy0Q62zcafi2KsZqtj4NiNb0N39CZbvTbZ6FN2zqb1TPW6xv/IRXH9M+NdPL9rcpRyTpYzcVoyjrQUb01MbfS1JP21LayrMwu+tznIjEBZs6S2KmcprGtC2a9qjGGVldJltnGTfGpNArVCgTGeXyNKzftRieXpuT9niBKQWraCuMFxMu0AsuuM+Vn+6Ogs3OqMf6kbl3HPwkvtvK61pBobEu4dAqLpXjFuvYG2iT6Jlf01bwGcp9gnv2fcrO1Q22U78mTPXsDJMqf4zLqm2WgbNRSz6Pdkh1r2gzbuqb9Rrx5tTlmwGMy23ZddUsZztt98Bb4PHvtWX6t6AyC89RbVoLG99N7xrHs4y69C41ZK55YWXjZ9FWGSHQx5DsBVD7UhEZzvOCVq4wzGQWjQuLWxu2gJLtsavwdk6I0nO56pmfOA1VBO3rSiSQDQ1r0UmjMuq1tnLFSX6K2hyLYisFm5prcZtBYzU0sstTgYv+7qCmVblUn8GV+RQ7i91iem8ZYCymo52Szbrbxj+CM6+hr7qHw37Uc/vWtQ3DqmGwE9m5Weq1PxyMWaoNTPL9bXlv3iJlqteUtnEU42WcmmMvbckvXPKaoKd91rmtMK5Khv/AGcZvjON710mc3qRA618O/2tsWjSs77A+tSdFb0fQE/y9iNbLMmq0JqSSgu/jO7v02EFKaIqo5/6fd74dz1FbfQk4EM2bZMf6z31IbLrqKZb9a25zKHy/QjaYxtl71rL0C8pbqK3XXOyz1Gsb+wPlv4tw8o8dXtFwevRmM90WynkKNiyM43rWxGcmPHfdut6PQKY+mmZdtbAOec/TozZAcu+2sctXvpqY86ZJrRSHyl9HTMmmtwFYzljK2zQYuP7G+8WWXxYzf3qo15axHju9V/p3u6iNDLGSccq7Wc/bFw+xcSMzpk6exQI3KLeg49ln1FVikXfjUnEGd1YymdaBTezalrYil2ROFRJLaiQG7P0EaZtMsougU70Ws3lUqNRr5w63OwY/wDRtMTRJ9Eyu21yKinroynDtAxLpW7vG9QY46qRazMMr7a8Y0lSs+Mi1q7aFiCXsThAXgm2stfWe/BWhfbMzv1rexCxcpKsr/xc7y6INeX+W4crMpv6ytKjUy52m3bEh78iFWU3eDer/U17UU1lN0+ts+PeU7smr1BSjejLNehJsawZW2KRvQ1CjO2pjfamDfqFF6Fqt4555bMxHWELaBhZ2VCklEzWkkGZ6GU2re8ZytndgPTWM/yYnbt1nP4LVcdiYyHe4zndTio1bpS7jnhl3rdvdA1fTlluOsVgMY22Nd0pjooCZbrQ0VEhtCDym9HbllL5bjePpFO1MjRzSDPur/lFaN7mxpuxztuOXBleDf8A6rOtXL/0eNGuKXqjXj+jMdQxS/5IqmPWvUItkEYyn6glbtljEgot6tmz9M2gZdGa+MzdrU5RDu/Vta2dT0iqVeXKPV/jOWW/Sw0W28jU/H+zhjqb+tqiFUV4yp2PJn5tS6irGvJb6xJbXTQmqK70u/IRGNVzyll676HjFHLGZTsdL33GiDM9M2TK941cd/VMJAEmMWuteMICFC0EWYdgQYhGLtY3bV9MTHURWixldUeYRu3TG2bdrYpOGO8bKcexqBrlrX/YuN3x0s3WpJFRzkq8f46UVKrE3v21theQOn/FmUxWCq9nGbxeWmfdEO2vHygk4pb8Ba16axxWM+1pN0WmcuG3UZ3Mv8b/APlMRztuVbxw+044ePv220LTOWUgyz1yMe/YOv0ZHY91lVFMYdfogYhv9LaoSxvvpqKJJm5aoNJmZbaApIRJDKioMnaEJGN2QRCUVYy5G6NTXUHOzc6PGn6RqDXB42tQzlKkWOPOt6SEWhbo1jK6oK5Dd+GK+uIrPxnXW9XQ+qDdhmYo+qLXkZNLfxrxtQZtOM01cdCdKN7Fqtc8sviZgssjhjNbrOGPlf47ajSKTTOd1NftqOWd3kDJkSEblP0LaNNb0rb+mN7rcoLZZqlBv0h8IiFhSjMmjF6SB2toKHbPute2cuBgvtfAtstHGtbZjSpo2zcv03qfVyCKeuoeyDGWhLpq47U/HBaJTjN9quMhnAaLO5s7EGXpj3WrRfQuKqemVvSLGqL1eQ3tUZ+t+Ck7tv4bozMem3UMVm0Gf93U3jJIx+TKTn1YVjK65Bjjcm8MObpysxVFNYwX8n6YttQG5WsnW0Ivqp9MqO2tM3nY3YzcWGmJtNWag8VDOzRxlgm5WtqGGMytRESqQC+hNtMgfg2b6G4KZVZtnfWtho8D4woQakH1pa6DNi01pAEVsBVBTAFrO5v2c5xnU1/RTb+jKwQautMq1m9oHY9nxv060KNdXjqtYxq8KihC31BdN9C2RyyyuV0uI6X8nzGboxw+3taxx1B+TLU1PdUZy/JrkY9qQiJJb4B3pfGY16UF6EgehlrSYVjLqpooo10yKEFTBppUSBAFIGMpxjVdaN/xFExrUgnWoCSKokkoLWfZq2gOnSiBI7ZuQKjkFyGxWrOsXla3qOe+qHZ+DH21fQKejIZFqMqdKlT9iCseUl9n8mWppy9rmJXTPshww12+xhj9roorfGbcLbld1v8AJlu6nxgQpBQqqRApFUAQSB6doe2bdM6rV9Mre1kiqNRzalF1d2di2jtBryNoka0Ip6VSEAp0NCmHY3oToHZ3r2LdONu8uriO5Zx9EEKdigJSxvqFOV0zf6qPoKRWtaPjso53amNrrMToozMdRSNQoMjRGwOPWhPQvbqKgyx8lMZGhllMYCZyzkmp7YuVt6FiI6UWwQSkUO0gCBAJJA7Sta2zjjuNaZUK+lYkVaPiPrWzAaHi1uDcVDJEEBQNAJb4xtFasUrPlxqWaUYzvWNbpz/3LD/tUdceQiTS2gWbRlWbuine6t9ZO+KK8E9r21jj+wMlt60tHTIikRArTXPK7qwVrPlqq3Swx3dqNyZX+NSSEArdTdcLfK7azy8r/GREitKHmhJtVTgLSVoBJAEkgSSB3x/2xpib0cWVaBFqaCzTNyNvBJvoouxtrKsaUdJkrkzPX9QNTIWiK3/0DtXoxjXAZ0pxuM1RjLtUxrQ3qiR0l4KMfXVleootkFyFGlRbUm61MbW5jIUWOOo1pQsqkPIXIQ7Fy0N7uorNKM3K1m39HK7uo1jhJ7UYxxtdZqRnPLx5Pbnu33QdrlJ9Yyz3NRhCI6Bih0LWryMgvi2qASQBJIEkgSSB6Ivqx9L6ypFhQMC3U1GsmKirS104qqDQPwS9BqCzVanofAH/AEYDFGvLjJrICjTU9j6ClHtVrEGbGp70q1E3QlQsiGSZytaQbG91fVAampGMsvkGVp/HN1RrDHU3fbWV1Npy/Jf8tAze0iFUSSgE60PgBu34ygBCoBJIEkgSSBJIH//Z'
                    ]
                ]
            ]
        ));
        $company->setLongitude(7.16432);
        $company->setLatitude(51.25690);

        $manager->persist($company);

        $company = new Company();

        $company->setValue(json_encode(
            [
                'companyName' => 'Wupper-Grill',
                'ownerName' => 'Wupper-Grill',
                'zip' => '42277',
                'city' => 'Wuppertal',
                'street' => 'Berliner Platz 3',
                'contactTimes' => 'Mo. bis Fr. 11:00 Uhr bis 00:00 Uhr; Sa.+So. 12:00 Uhr bis 00:00 Uhr',
                'phone' => '+49 202 8978083',
                'email' => '',
                'facebook' => 'https://www.facebook.com/pages/Wupper-Grill/161471053873312',
                'web' => '',
                'description' => '',
                'images' => [
                    [
                        'type' => 'business',
                        'base64' => 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAKBueIx4ZKCMgoy0qqC+8P//8Nzc8P//////////////////////////////////////////////////////////2wBDAaq0tPDS8P//////////////////////////////////////////////////////////////////////////////wAARCAEsAZADASIAAhEBAxEB/8QAGAABAQEBAQAAAAAAAAAAAAAAAAECAwT/xAAmEAEBAAEEAgIDAQEBAQEAAAAAARECITFBUWEDEjJxgSJCkVLw/8QAFQEBAQAAAAAAAAAAAAAAAAAAAAH/xAAVEQEBAAAAAAAAAAAAAAAAAAAAAf/aAAwDAQACEQMRAD8A6gAAAAAAAAoIAAAAKgAAAAAAAAAAAAAAAKAAAAAAAAAAAAACAAAAAAAAAAAAAAAoAIACoAAAAAAAAAAAAMavkk2m9BscZ8mqRZ8lxNgdVYnyab21NUvcBQAAAAAAAAAAQAAAAAEAVFAAABQQAAABQBAABQEFQAAARQAZ1app5BpnVrmn9uerXbxtGQXVrur1EEyCmUAUwmTILMzitffVO6zkyDc12TDfx6rqly4Za0avrcg9AigCAKgAAAAAAgAACooAAAAAKCAAoAAAAIAAABbJyAlsk3rGr5P/AJ/9c7bQb1fJbxswhkFS1FABqQEkS7VtLAZDAAGVxQQi2bIDt8WrMx4befTcXL0S5mYAAAAAAACAAAAAAAoAAAAAAACooAAAIAM6vkk43crquoHTV8knDlbbyJkFTIAigAGN1kt42A08tE04UBFTMtBLpT63y2vQOeL2q2bw6AYvLbOrkEdPi1f83+OSy4B6RNOr7TKgAgCoAqAAAAAAAAqAKAAAAAAogKMatc0uerXdQOmrXJ7rnq13UymQVLTkAMDWmbAmEsbvKXmAy1JtvF0yYzDVwCdtaN5lM4qS2TE2BqpnwmPJkDGeabdJyAu/lc30mb4Pt6Bbb4TMMwzPIGYmrfFXaICIt4Ab+PVi46rs80d9Gr7afYKAAAAAAAAAAAAACgAAAAzq1zT+wXVqmmbuN16r2a9f2ZyCpkAAXTAMJhu8J2DM34jc4XTwAnZ2md9jGeUF03GnHZvfSZkM28RRcSJb4MeTZBMW8qoBhPZy10CFLxswo0YjO8alz+wTB+2uUwCXhMLzNgGW/j1YvpnVygPSOfx683F/joAAAAAAAAAAAACgAAAzqlsxLhx1SzmPQlksxQeZWsYTGwIuNiaVoJNLUmEn5T0tv9AqXs3ptAJbjEMeTOQD7Trc3pphbgCSNOc1VtBLcMctat6mLbiKLpalyk0zCyYAzuvSXlUGcZT6e2ycqOe85WbVu7RjnFBqUpgqCXbdD/o4uOlDV0y1eMJeQTLejVjV6YAekY+PVmY7jYAAAgAACgAAAKgCgAJquNNLqx7YurPYM3hKWm99Asxm1DacmfAGPJmGLeTAJvauIRm3IN9IaOGrwgnTF3rWLU/aiSW3ZblrTvC9AzJ/prTc6sE/JdE7Bai1ATG6peVQCchOVDVM5jOn8f61eWZzqBroJwIM3mVbwmraRKoc7dpV7L5BEsU1dAadWLl3lzHndPj1dA6AAAAAAoAAAAjOrXjoG6zbJzf4535LWbcg6XVL4ZYm7QLmTg3vok7T7bgv18luFrFBvTcwwmnOGrwgznbCYzFsZs3kUbmnC0KgdM38a1eGdX4/1RdE/wAl5izbTGbdwWfkum7MfZN6DpdUSXNZmny1ICgIM2VM1rMMAz9qZawYUSallySCBeKzY3eGeYohwSLef2CF/E9ICEuKvaA9Gm5mRz+LVi48uoIKAgqAo5TVLzVurTPYOl1Sc2Mavkk4c7qyyDWrXazkADCkm4LJupAC8M4vUWxdPOPAJc7nEytLzIDU4KFQSpz8i9s5xqyo2lWcJqmagl1RLcr9YuJAZktammGTIGDaM2m4NZJc1nFWSSg0ADNu6Z9NXnpP4Bkz7P4bKGVz+kxPJJvyg10xxcNxmzdQheInFxWuqDN3qYXsAS8rdqlBJtXpnEeau3x6s6ceAbAAQAcMVGsFBnCkAISbr0dAkanCdNdSAHYdAnZp4tOqku2IC3kz/rKb0mnyDcuWdWc7NREGcWrNJkyDSW7qzZvyBamV2/Zn9QExauJ3UznszFF26hn9RnNvaA1mftZzwzp5b2gKJ9p0n2vUQWzdi8tS3OK1hRzz7XN8tY9H1ngGc+l/iY2/rQLGdXLSagZu66b0nH6WcglIt5wgF4SeGumaBeF+PVjUnMTsHpRNNzplUAAHKJVkxE5oHRjeReyd0EvK3wTk5oC9pOasAu0ZuWqmQT6tbSM5NwaymUx5qyTPANMYtbvDFvpAxP2s54ZzfK6OVG2by0kQYtqN6uU+uaok54ax/qk078r3QZxyk03HDU7NNtoEmKXHZ/3VsyDOZ4M1rEVBmctCKLN6ZNPJeaCXfJ1sT8v4dAzjyXjHS4LMfoEhwY2SUGtU4qLfxZvkGmWksBnit6NMurdlrTcaoDtjAAIKA43ZIWnQLODoxxDUCTsnNpxpJ+ILOFgvQM3e8pifs3Z3BvKW49pp3yWbgZv6b0xj63LVz5Bq8MUxFoM/WtYsmyb5wtmQTE7q6TCyIJ2lty1OUnMUW/md0v5VJOQJxTRyT8b+jRyC/wDVD/qgKAgIooTk1ci6uQZ/6hKXmL3QRegiDM7jMm7XGqnaizixnhc2XhIAXgi8wGaHGyg7abnTlWPit3joCCoDz1eyeVnGQJzlLvcL0QC707wQndBf2l1RNs8HfALeEwtMcQDGE5q27k4tA03OT6mnhoGcbna1ASfk3GNLYFTC0QScpp/KLsZ0zdRP+qs7T7TwluQaxtTTpxc1n+FBrVtvLumfaTKybgZvlpPSgluD7LUsQPuv3Z+qYUb+08JndnBig2RnF8tRBnV+QurpKoXKXeKtBiNRDIJSF3AdPjuNX7dXCXFldgVABx6L1DsnIFpxDml5wBxDoq9gSHapOATtZzUnK7TsEvBfxLYfb0CziKzLbWgRNkxTALmTiNMzS1meQTUzu1RBjdcNYXAMzSuNlAZzthcHZlQwUvKAf2mJ4tXN8GfcAx6VnPtc30gomb4XPoBKufSZApm+DJt5BLczHaStbJjdQi3hjeVqXMBKjVQEo10zgFnDrpudMcY6fHdrAbABy6OjM9mb4A0xcbpnV6hi+QXsnKfUm3QLq4Z3XOauAZ3WaVwoMfVfquyoJwW/o/iZ/Siqz9vafb9oNH9Zz6M3wo1n2Z9M7+U/oN5T7e4zsA19vaZntP4f+AufRmpn2Z9g1qznZN/Jr5T+Af02ADZcRN1AxPJj2W+kzPALv5XfyzmGYDW6Zpn9n9Aze1lZl9qDV3IzmzleQTONqLjLM2oNRFS8gjWi/wCv2yvGAdgm8AcwxfJid0AzD/M7JZeATKyemdTX3k7tBcUx7Z+86h971AaxPa4nhz++pM6vIOv8Z1a8XDG97TALzeAQF/8ADPtkBoIUEz6XcAP6i5x0QAwABlAG9XLLWrpnigoAAAFT6+16AZAgLFOklBSgCC4i4kgJku56wWYAS5MmQUCA6fHc6f025/Hd7GwcMrJmJWpwFTvEX/U6TTuvQM3Od07W81JyCgdAbZ2CcALNkAGpoth9PbXaXj/96Bn6XyTR7b5uP2luJ+gY7wUtzaApj3BAaxPKXGdkyAAACKDWrpldfEZgKCgAAAgFIiwFRQEUAXVwXhM7LOgTldQW5BhTsvIBnCL0DWi41R0zPLjOWsg//9k='
                    ]
                ]
            ]
        ));
        $company->setLongitude(7.22296);
        $company->setLatitude(51.27495);

        $manager->persist($company);

        $company = new Company();

        $company->setValue(json_encode(
            [
                'companyName' => 'Zweirad Otto',
                'ownerName' => 'Jörg Otto e.K.',
                'zip' => '42115',
                'city' => 'Wuppertal',
                'street' => 'Bayreuther Straße 91',
                'contactTimes' => 'Mo. bis Sa. 08:00 Uhr bis 17:00 Uhr',
                'phone' => '+49 202 304455',
                'email' => 'info@piaggio-otto.de',
                'facebook' => '',
                'web' => '',
                'description' => '',
                'images' => [
                    [
                        'type' => 'business',
                        'base64' => 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAKBueIx4ZKCMgoy0qqC+8P//8Nzc8P//////////////////////////////////////////////////////////2wBDAaq0tPDS8P//////////////////////////////////////////////////////////////////////////////wAARCAEsAZADASIAAhEBAxEB/8QAGAABAQEBAQAAAAAAAAAAAAAAAAECAwT/xAAmEAEBAAEEAgIDAQEBAQEAAAAAARECITFBUWEDEjJxgSJCkVLw/8QAFQEBAQAAAAAAAAAAAAAAAAAAAAH/xAAVEQEBAAAAAAAAAAAAAAAAAAAAAf/aAAwDAQACEQMRAD8A6gAAAAAAAAoIAAAAKgAAAAAAAAAAAAAAAKAAAAAAAAAAAAACAAAAAAAAAAAAAAAoAIACoAAAAAAAAAAAAMavkk2m9BscZ8mqRZ8lxNgdVYnyab21NUvcBQAAAAAAAAAAQAAAAAEAVFAAABQQAAABQBAABQEFQAAARQAZ1app5BpnVrmn9uerXbxtGQXVrur1EEyCmUAUwmTILMzitffVO6zkyDc12TDfx6rqly4Za0avrcg9AigCAKgAAAAAAgAACooAAAAAKCAAoAAAAIAAABbJyAlsk3rGr5P/AJ/9c7bQb1fJbxswhkFS1FABqQEkS7VtLAZDAAGVxQQi2bIDt8WrMx4befTcXL0S5mYAAAAAAACAAAAAAAoAAAAAAACooAAAIAM6vkk43crquoHTV8knDlbbyJkFTIAigAGN1kt42A08tE04UBFTMtBLpT63y2vQOeL2q2bw6AYvLbOrkEdPi1f83+OSy4B6RNOr7TKgAgCoAqAAAAAAAAqAKAAAAAAogKMatc0uerXdQOmrXJ7rnq13UymQVLTkAMDWmbAmEsbvKXmAy1JtvF0yYzDVwCdtaN5lM4qS2TE2BqpnwmPJkDGeabdJyAu/lc30mb4Pt6Bbb4TMMwzPIGYmrfFXaICIt4Ab+PVi46rs80d9Gr7afYKAAAAAAAAAAAAACgAAAAzq1zT+wXVqmmbuN16r2a9f2ZyCpkAAXTAMJhu8J2DM34jc4XTwAnZ2md9jGeUF03GnHZvfSZkM28RRcSJb4MeTZBMW8qoBhPZy10CFLxswo0YjO8alz+wTB+2uUwCXhMLzNgGW/j1YvpnVygPSOfx683F/joAAAAAAAAAAAACgAAAzqlsxLhx1SzmPQlksxQeZWsYTGwIuNiaVoJNLUmEn5T0tv9AqXs3ptAJbjEMeTOQD7Trc3pphbgCSNOc1VtBLcMctat6mLbiKLpalyk0zCyYAzuvSXlUGcZT6e2ycqOe85WbVu7RjnFBqUpgqCXbdD/o4uOlDV0y1eMJeQTLejVjV6YAekY+PVmY7jYAAAgAACgAAAKgCgAJquNNLqx7YurPYM3hKWm99Asxm1DacmfAGPJmGLeTAJvauIRm3IN9IaOGrwgnTF3rWLU/aiSW3ZblrTvC9AzJ/prTc6sE/JdE7Bai1ATG6peVQCchOVDVM5jOn8f61eWZzqBroJwIM3mVbwmraRKoc7dpV7L5BEsU1dAadWLl3lzHndPj1dA6AAAAAAoAAAAjOrXjoG6zbJzf4535LWbcg6XVL4ZYm7QLmTg3vok7T7bgv18luFrFBvTcwwmnOGrwgznbCYzFsZs3kUbmnC0KgdM38a1eGdX4/1RdE/wAl5izbTGbdwWfkum7MfZN6DpdUSXNZmny1ICgIM2VM1rMMAz9qZawYUSallySCBeKzY3eGeYohwSLef2CF/E9ICEuKvaA9Gm5mRz+LVi48uoIKAgqAo5TVLzVurTPYOl1Sc2Mavkk4c7qyyDWrXazkADCkm4LJupAC8M4vUWxdPOPAJc7nEytLzIDU4KFQSpz8i9s5xqyo2lWcJqmagl1RLcr9YuJAZktammGTIGDaM2m4NZJc1nFWSSg0ADNu6Z9NXnpP4Bkz7P4bKGVz+kxPJJvyg10xxcNxmzdQheInFxWuqDN3qYXsAS8rdqlBJtXpnEeau3x6s6ceAbAAQAcMVGsFBnCkAISbr0dAkanCdNdSAHYdAnZp4tOqku2IC3kz/rKb0mnyDcuWdWc7NREGcWrNJkyDSW7qzZvyBamV2/Zn9QExauJ3UznszFF26hn9RnNvaA1mftZzwzp5b2gKJ9p0n2vUQWzdi8tS3OK1hRzz7XN8tY9H1ngGc+l/iY2/rQLGdXLSagZu66b0nH6WcglIt5wgF4SeGumaBeF+PVjUnMTsHpRNNzplUAAHKJVkxE5oHRjeReyd0EvK3wTk5oC9pOasAu0ZuWqmQT6tbSM5NwaymUx5qyTPANMYtbvDFvpAxP2s54ZzfK6OVG2by0kQYtqN6uU+uaok54ax/qk078r3QZxyk03HDU7NNtoEmKXHZ/3VsyDOZ4M1rEVBmctCKLN6ZNPJeaCXfJ1sT8v4dAzjyXjHS4LMfoEhwY2SUGtU4qLfxZvkGmWksBnit6NMurdlrTcaoDtjAAIKA43ZIWnQLODoxxDUCTsnNpxpJ+ILOFgvQM3e8pifs3Z3BvKW49pp3yWbgZv6b0xj63LVz5Bq8MUxFoM/WtYsmyb5wtmQTE7q6TCyIJ2lty1OUnMUW/md0v5VJOQJxTRyT8b+jRyC/wDVD/qgKAgIooTk1ci6uQZ/6hKXmL3QRegiDM7jMm7XGqnaizixnhc2XhIAXgi8wGaHGyg7abnTlWPit3joCCoDz1eyeVnGQJzlLvcL0QC707wQndBf2l1RNs8HfALeEwtMcQDGE5q27k4tA03OT6mnhoGcbna1ASfk3GNLYFTC0QScpp/KLsZ0zdRP+qs7T7TwluQaxtTTpxc1n+FBrVtvLumfaTKybgZvlpPSgluD7LUsQPuv3Z+qYUb+08JndnBig2RnF8tRBnV+QurpKoXKXeKtBiNRDIJSF3AdPjuNX7dXCXFldgVABx6L1DsnIFpxDml5wBxDoq9gSHapOATtZzUnK7TsEvBfxLYfb0CziKzLbWgRNkxTALmTiNMzS1meQTUzu1RBjdcNYXAMzSuNlAZzthcHZlQwUvKAf2mJ4tXN8GfcAx6VnPtc30gomb4XPoBKufSZApm+DJt5BLczHaStbJjdQi3hjeVqXMBKjVQEo10zgFnDrpudMcY6fHdrAbABy6OjM9mb4A0xcbpnV6hi+QXsnKfUm3QLq4Z3XOauAZ3WaVwoMfVfquyoJwW/o/iZ/Siqz9vafb9oNH9Zz6M3wo1n2Z9M7+U/oN5T7e4zsA19vaZntP4f+AufRmpn2Z9g1qznZN/Jr5T+Af02ADZcRN1AxPJj2W+kzPALv5XfyzmGYDW6Zpn9n9Aze1lZl9qDV3IzmzleQTONqLjLM2oNRFS8gjWi/wCv2yvGAdgm8AcwxfJid0AzD/M7JZeATKyemdTX3k7tBcUx7Z+86h971AaxPa4nhz++pM6vIOv8Z1a8XDG97TALzeAQF/8ADPtkBoIUEz6XcAP6i5x0QAwABlAG9XLLWrpnigoAAAFT6+16AZAgLFOklBSgCC4i4kgJku56wWYAS5MmQUCA6fHc6f025/Hd7GwcMrJmJWpwFTvEX/U6TTuvQM3Od07W81JyCgdAbZ2CcALNkAGpoth9PbXaXj/96Bn6XyTR7b5uP2luJ+gY7wUtzaApj3BAaxPKXGdkyAAACKDWrpldfEZgKCgAAAgFIiwFRQEUAXVwXhM7LOgTldQW5BhTsvIBnCL0DWi41R0zPLjOWsg//9k='
                    ]
                ]
            ]
        ));
        $company->setLongitude(7.12624);
        $company->setLatitude(51.26035);

        $manager->persist($company);

        $company = new Company();

        $company->setValue(json_encode(
            [
                'companyName' => 'Buchhandlung Schleu',
                'ownerName' => 'Ursula Schleu-Behle',
                'zip' => '42277',
                'city' => 'Wuppertal',
                'street' => 'Berliner Str. 158',
                'contactTimes' => 'Mo - Fr   9.00 - 18.00 Uhr; Mi + Sa  9.00 - 13.00 Uhr',
                'phone' => '+49 202 660977',
                'email' => 'Buchhandlung-Schleu@t-online.de',
                'facebook' => '',
                'web' => '',
                'description' => '',
                'images' => [
                    [
                        'type' => 'business',
                        'base64' => 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAKBueIx4ZKCMgoy0qqC+8P//8Nzc8P//////////////////////////////////////////////////////////2wBDAaq0tPDS8P//////////////////////////////////////////////////////////////////////////////wAARCAEsAZADASIAAhEBAxEB/8QAGAABAQEBAQAAAAAAAAAAAAAAAAECAwT/xAAmEAEBAAEEAgIDAQEBAQEAAAAAARECITFBUWEDEjJxgSJCkVLw/8QAFQEBAQAAAAAAAAAAAAAAAAAAAAH/xAAVEQEBAAAAAAAAAAAAAAAAAAAAAf/aAAwDAQACEQMRAD8A6gAAAAAAAAoIAAAAKgAAAAAAAAAAAAAAAKAAAAAAAAAAAAACAAAAAAAAAAAAAAAoAIACoAAAAAAAAAAAAMavkk2m9BscZ8mqRZ8lxNgdVYnyab21NUvcBQAAAAAAAAAAQAAAAAEAVFAAABQQAAABQBAABQEFQAAARQAZ1app5BpnVrmn9uerXbxtGQXVrur1EEyCmUAUwmTILMzitffVO6zkyDc12TDfx6rqly4Za0avrcg9AigCAKgAAAAAAgAACooAAAAAKCAAoAAAAIAAABbJyAlsk3rGr5P/AJ/9c7bQb1fJbxswhkFS1FABqQEkS7VtLAZDAAGVxQQi2bIDt8WrMx4befTcXL0S5mYAAAAAAACAAAAAAAoAAAAAAACooAAAIAM6vkk43crquoHTV8knDlbbyJkFTIAigAGN1kt42A08tE04UBFTMtBLpT63y2vQOeL2q2bw6AYvLbOrkEdPi1f83+OSy4B6RNOr7TKgAgCoAqAAAAAAAAqAKAAAAAAogKMatc0uerXdQOmrXJ7rnq13UymQVLTkAMDWmbAmEsbvKXmAy1JtvF0yYzDVwCdtaN5lM4qS2TE2BqpnwmPJkDGeabdJyAu/lc30mb4Pt6Bbb4TMMwzPIGYmrfFXaICIt4Ab+PVi46rs80d9Gr7afYKAAAAAAAAAAAAACgAAAAzq1zT+wXVqmmbuN16r2a9f2ZyCpkAAXTAMJhu8J2DM34jc4XTwAnZ2md9jGeUF03GnHZvfSZkM28RRcSJb4MeTZBMW8qoBhPZy10CFLxswo0YjO8alz+wTB+2uUwCXhMLzNgGW/j1YvpnVygPSOfx683F/joAAAAAAAAAAAACgAAAzqlsxLhx1SzmPQlksxQeZWsYTGwIuNiaVoJNLUmEn5T0tv9AqXs3ptAJbjEMeTOQD7Trc3pphbgCSNOc1VtBLcMctat6mLbiKLpalyk0zCyYAzuvSXlUGcZT6e2ycqOe85WbVu7RjnFBqUpgqCXbdD/o4uOlDV0y1eMJeQTLejVjV6YAekY+PVmY7jYAAAgAACgAAAKgCgAJquNNLqx7YurPYM3hKWm99Asxm1DacmfAGPJmGLeTAJvauIRm3IN9IaOGrwgnTF3rWLU/aiSW3ZblrTvC9AzJ/prTc6sE/JdE7Bai1ATG6peVQCchOVDVM5jOn8f61eWZzqBroJwIM3mVbwmraRKoc7dpV7L5BEsU1dAadWLl3lzHndPj1dA6AAAAAAoAAAAjOrXjoG6zbJzf4535LWbcg6XVL4ZYm7QLmTg3vok7T7bgv18luFrFBvTcwwmnOGrwgznbCYzFsZs3kUbmnC0KgdM38a1eGdX4/1RdE/wAl5izbTGbdwWfkum7MfZN6DpdUSXNZmny1ICgIM2VM1rMMAz9qZawYUSallySCBeKzY3eGeYohwSLef2CF/E9ICEuKvaA9Gm5mRz+LVi48uoIKAgqAo5TVLzVurTPYOl1Sc2Mavkk4c7qyyDWrXazkADCkm4LJupAC8M4vUWxdPOPAJc7nEytLzIDU4KFQSpz8i9s5xqyo2lWcJqmagl1RLcr9YuJAZktammGTIGDaM2m4NZJc1nFWSSg0ADNu6Z9NXnpP4Bkz7P4bKGVz+kxPJJvyg10xxcNxmzdQheInFxWuqDN3qYXsAS8rdqlBJtXpnEeau3x6s6ceAbAAQAcMVGsFBnCkAISbr0dAkanCdNdSAHYdAnZp4tOqku2IC3kz/rKb0mnyDcuWdWc7NREGcWrNJkyDSW7qzZvyBamV2/Zn9QExauJ3UznszFF26hn9RnNvaA1mftZzwzp5b2gKJ9p0n2vUQWzdi8tS3OK1hRzz7XN8tY9H1ngGc+l/iY2/rQLGdXLSagZu66b0nH6WcglIt5wgF4SeGumaBeF+PVjUnMTsHpRNNzplUAAHKJVkxE5oHRjeReyd0EvK3wTk5oC9pOasAu0ZuWqmQT6tbSM5NwaymUx5qyTPANMYtbvDFvpAxP2s54ZzfK6OVG2by0kQYtqN6uU+uaok54ax/qk078r3QZxyk03HDU7NNtoEmKXHZ/3VsyDOZ4M1rEVBmctCKLN6ZNPJeaCXfJ1sT8v4dAzjyXjHS4LMfoEhwY2SUGtU4qLfxZvkGmWksBnit6NMurdlrTcaoDtjAAIKA43ZIWnQLODoxxDUCTsnNpxpJ+ILOFgvQM3e8pifs3Z3BvKW49pp3yWbgZv6b0xj63LVz5Bq8MUxFoM/WtYsmyb5wtmQTE7q6TCyIJ2lty1OUnMUW/md0v5VJOQJxTRyT8b+jRyC/wDVD/qgKAgIooTk1ci6uQZ/6hKXmL3QRegiDM7jMm7XGqnaizixnhc2XhIAXgi8wGaHGyg7abnTlWPit3joCCoDz1eyeVnGQJzlLvcL0QC707wQndBf2l1RNs8HfALeEwtMcQDGE5q27k4tA03OT6mnhoGcbna1ASfk3GNLYFTC0QScpp/KLsZ0zdRP+qs7T7TwluQaxtTTpxc1n+FBrVtvLumfaTKybgZvlpPSgluD7LUsQPuv3Z+qYUb+08JndnBig2RnF8tRBnV+QurpKoXKXeKtBiNRDIJSF3AdPjuNX7dXCXFldgVABx6L1DsnIFpxDml5wBxDoq9gSHapOATtZzUnK7TsEvBfxLYfb0CziKzLbWgRNkxTALmTiNMzS1meQTUzu1RBjdcNYXAMzSuNlAZzthcHZlQwUvKAf2mJ4tXN8GfcAx6VnPtc30gomb4XPoBKufSZApm+DJt5BLczHaStbJjdQi3hjeVqXMBKjVQEo10zgFnDrpudMcY6fHdrAbABy6OjM9mb4A0xcbpnV6hi+QXsnKfUm3QLq4Z3XOauAZ3WaVwoMfVfquyoJwW/o/iZ/Siqz9vafb9oNH9Zz6M3wo1n2Z9M7+U/oN5T7e4zsA19vaZntP4f+AufRmpn2Z9g1qznZN/Jr5T+Af02ADZcRN1AxPJj2W+kzPALv5XfyzmGYDW6Zpn9n9Aze1lZl9qDV3IzmzleQTONqLjLM2oNRFS8gjWi/wCv2yvGAdgm8AcwxfJid0AzD/M7JZeATKyemdTX3k7tBcUx7Z+86h971AaxPa4nhz++pM6vIOv8Z1a8XDG97TALzeAQF/8ADPtkBoIUEz6XcAP6i5x0QAwABlAG9XLLWrpnigoAAAFT6+16AZAgLFOklBSgCC4i4kgJku56wWYAS5MmQUCA6fHc6f025/Hd7GwcMrJmJWpwFTvEX/U6TTuvQM3Od07W81JyCgdAbZ2CcALNkAGpoth9PbXaXj/96Bn6XyTR7b5uP2luJ+gY7wUtzaApj3BAaxPKXGdkyAAACKDWrpldfEZgKCgAAAgFIiwFRQEUAXVwXhM7LOgTldQW5BhTsvIBnCL0DWi41R0zPLjOWsg//9k='
                    ]
                ]
            ]
        ));
        $company->setLongitude(7.22007);
        $company->setLatitude(51.27508);

        $manager->persist($company);

        $company = new Company();

        $company->setValue(json_encode(
            [
                'companyName' => 'Bäckerei B.Schmalz',
                'ownerName' => 'B.Schmalz',
                'zip' => '40625',
                'city' => 'Düsseldorf',
                'street' => 'Benderstraße 99',
                'contactTimes' => 'Mo-Fr 06:00-18:30 Uhr; Sa 06.00-13.00 Uhr; So 08.00-12.00 Uhr',
                'phone' => '+49 211 297989',
                'email' => '',
                'facebook' => '',
                'web' => '',
                'description' => '',
                'images' => [
                    [
                        'type' => 'business',
                        'base64' => 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAKBueIx4ZKCMgoy0qqC+8P//8Nzc8P//////////////////////////////////////////////////////////2wBDAaq0tPDS8P//////////////////////////////////////////////////////////////////////////////wAARCAEsAZADASIAAhEBAxEB/8QAGAABAQEBAQAAAAAAAAAAAAAAAAECAwT/xAAmEAEBAAEEAgIDAQEBAQEAAAAAARECITFBUWEDEjJxgSJCkVLw/8QAFQEBAQAAAAAAAAAAAAAAAAAAAAH/xAAVEQEBAAAAAAAAAAAAAAAAAAAAAf/aAAwDAQACEQMRAD8A6gAAAAAAAAoIAAAAKgAAAAAAAAAAAAAAAKAAAAAAAAAAAAACAAAAAAAAAAAAAAAoAIACoAAAAAAAAAAAAMavkk2m9BscZ8mqRZ8lxNgdVYnyab21NUvcBQAAAAAAAAAAQAAAAAEAVFAAABQQAAABQBAABQEFQAAARQAZ1app5BpnVrmn9uerXbxtGQXVrur1EEyCmUAUwmTILMzitffVO6zkyDc12TDfx6rqly4Za0avrcg9AigCAKgAAAAAAgAACooAAAAAKCAAoAAAAIAAABbJyAlsk3rGr5P/AJ/9c7bQb1fJbxswhkFS1FABqQEkS7VtLAZDAAGVxQQi2bIDt8WrMx4befTcXL0S5mYAAAAAAACAAAAAAAoAAAAAAACooAAAIAM6vkk43crquoHTV8knDlbbyJkFTIAigAGN1kt42A08tE04UBFTMtBLpT63y2vQOeL2q2bw6AYvLbOrkEdPi1f83+OSy4B6RNOr7TKgAgCoAqAAAAAAAAqAKAAAAAAogKMatc0uerXdQOmrXJ7rnq13UymQVLTkAMDWmbAmEsbvKXmAy1JtvF0yYzDVwCdtaN5lM4qS2TE2BqpnwmPJkDGeabdJyAu/lc30mb4Pt6Bbb4TMMwzPIGYmrfFXaICIt4Ab+PVi46rs80d9Gr7afYKAAAAAAAAAAAAACgAAAAzq1zT+wXVqmmbuN16r2a9f2ZyCpkAAXTAMJhu8J2DM34jc4XTwAnZ2md9jGeUF03GnHZvfSZkM28RRcSJb4MeTZBMW8qoBhPZy10CFLxswo0YjO8alz+wTB+2uUwCXhMLzNgGW/j1YvpnVygPSOfx683F/joAAAAAAAAAAAACgAAAzqlsxLhx1SzmPQlksxQeZWsYTGwIuNiaVoJNLUmEn5T0tv9AqXs3ptAJbjEMeTOQD7Trc3pphbgCSNOc1VtBLcMctat6mLbiKLpalyk0zCyYAzuvSXlUGcZT6e2ycqOe85WbVu7RjnFBqUpgqCXbdD/o4uOlDV0y1eMJeQTLejVjV6YAekY+PVmY7jYAAAgAACgAAAKgCgAJquNNLqx7YurPYM3hKWm99Asxm1DacmfAGPJmGLeTAJvauIRm3IN9IaOGrwgnTF3rWLU/aiSW3ZblrTvC9AzJ/prTc6sE/JdE7Bai1ATG6peVQCchOVDVM5jOn8f61eWZzqBroJwIM3mVbwmraRKoc7dpV7L5BEsU1dAadWLl3lzHndPj1dA6AAAAAAoAAAAjOrXjoG6zbJzf4535LWbcg6XVL4ZYm7QLmTg3vok7T7bgv18luFrFBvTcwwmnOGrwgznbCYzFsZs3kUbmnC0KgdM38a1eGdX4/1RdE/wAl5izbTGbdwWfkum7MfZN6DpdUSXNZmny1ICgIM2VM1rMMAz9qZawYUSallySCBeKzY3eGeYohwSLef2CF/E9ICEuKvaA9Gm5mRz+LVi48uoIKAgqAo5TVLzVurTPYOl1Sc2Mavkk4c7qyyDWrXazkADCkm4LJupAC8M4vUWxdPOPAJc7nEytLzIDU4KFQSpz8i9s5xqyo2lWcJqmagl1RLcr9YuJAZktammGTIGDaM2m4NZJc1nFWSSg0ADNu6Z9NXnpP4Bkz7P4bKGVz+kxPJJvyg10xxcNxmzdQheInFxWuqDN3qYXsAS8rdqlBJtXpnEeau3x6s6ceAbAAQAcMVGsFBnCkAISbr0dAkanCdNdSAHYdAnZp4tOqku2IC3kz/rKb0mnyDcuWdWc7NREGcWrNJkyDSW7qzZvyBamV2/Zn9QExauJ3UznszFF26hn9RnNvaA1mftZzwzp5b2gKJ9p0n2vUQWzdi8tS3OK1hRzz7XN8tY9H1ngGc+l/iY2/rQLGdXLSagZu66b0nH6WcglIt5wgF4SeGumaBeF+PVjUnMTsHpRNNzplUAAHKJVkxE5oHRjeReyd0EvK3wTk5oC9pOasAu0ZuWqmQT6tbSM5NwaymUx5qyTPANMYtbvDFvpAxP2s54ZzfK6OVG2by0kQYtqN6uU+uaok54ax/qk078r3QZxyk03HDU7NNtoEmKXHZ/3VsyDOZ4M1rEVBmctCKLN6ZNPJeaCXfJ1sT8v4dAzjyXjHS4LMfoEhwY2SUGtU4qLfxZvkGmWksBnit6NMurdlrTcaoDtjAAIKA43ZIWnQLODoxxDUCTsnNpxpJ+ILOFgvQM3e8pifs3Z3BvKW49pp3yWbgZv6b0xj63LVz5Bq8MUxFoM/WtYsmyb5wtmQTE7q6TCyIJ2lty1OUnMUW/md0v5VJOQJxTRyT8b+jRyC/wDVD/qgKAgIooTk1ci6uQZ/6hKXmL3QRegiDM7jMm7XGqnaizixnhc2XhIAXgi8wGaHGyg7abnTlWPit3joCCoDz1eyeVnGQJzlLvcL0QC707wQndBf2l1RNs8HfALeEwtMcQDGE5q27k4tA03OT6mnhoGcbna1ASfk3GNLYFTC0QScpp/KLsZ0zdRP+qs7T7TwluQaxtTTpxc1n+FBrVtvLumfaTKybgZvlpPSgluD7LUsQPuv3Z+qYUb+08JndnBig2RnF8tRBnV+QurpKoXKXeKtBiNRDIJSF3AdPjuNX7dXCXFldgVABx6L1DsnIFpxDml5wBxDoq9gSHapOATtZzUnK7TsEvBfxLYfb0CziKzLbWgRNkxTALmTiNMzS1meQTUzu1RBjdcNYXAMzSuNlAZzthcHZlQwUvKAf2mJ4tXN8GfcAx6VnPtc30gomb4XPoBKufSZApm+DJt5BLczHaStbJjdQi3hjeVqXMBKjVQEo10zgFnDrpudMcY6fHdrAbABy6OjM9mb4A0xcbpnV6hi+QXsnKfUm3QLq4Z3XOauAZ3WaVwoMfVfquyoJwW/o/iZ/Siqz9vafb9oNH9Zz6M3wo1n2Z9M7+U/oN5T7e4zsA19vaZntP4f+AufRmpn2Z9g1qznZN/Jr5T+Af02ADZcRN1AxPJj2W+kzPALv5XfyzmGYDW6Zpn9n9Aze1lZl9qDV3IzmzleQTONqLjLM2oNRFS8gjWi/wCv2yvGAdgm8AcwxfJid0AzD/M7JZeATKyemdTX3k7tBcUx7Z+86h971AaxPa4nhz++pM6vIOv8Z1a8XDG97TALzeAQF/8ADPtkBoIUEz6XcAP6i5x0QAwABlAG9XLLWrpnigoAAAFT6+16AZAgLFOklBSgCC4i4kgJku56wWYAS5MmQUCA6fHc6f025/Hd7GwcMrJmJWpwFTvEX/U6TTuvQM3Od07W81JyCgdAbZ2CcALNkAGpoth9PbXaXj/96Bn6XyTR7b5uP2luJ+gY7wUtzaApj3BAaxPKXGdkyAAACKDWrpldfEZgKCgAAAgFIiwFRQEUAXVwXhM7LOgTldQW5BhTsvIBnCL0DWi41R0zPLjOWsg//9k='
                    ]
                ]
            ]
        ));
        $company->setLongitude(6.84979);
        $company->setLatitude(51.23865);

        $manager->persist($company);
        $manager->flush();
    }
}
