<?php

namespace App\Command;


use Doctrine\DBAL\Connection;
use Doctrine\DBAL\DBALException;
use http\Exception\RuntimeException;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CreateSchemaDoctrineCommand extends \Doctrine\Bundle\DoctrineBundle\Command\Proxy\CreateSchemaDoctrineCommand
{
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $return =  parent::execute($input, $output);

        /** @var Connection $connection */
        $connection = $this->getApplication()->getKernel()->getContainer()->get('doctrine')->getConnection();

        try {
            $stmt = <<<EOT
CREATE OR REPLACE FUNCTION company_fill_coordinate() RETURNS TRIGGER AS
$$
   BEGIN
      new.coordinate = ST_GeogFromText('SRID=4326;POINT('||to_char(new.longitude,'999.99999')||' '||to_char(new.latitude,'999.99999')||')');
      RETURN NEW;
   END;
$$ LANGUAGE 'plpgsql';
EOT;
            $connection->executeQuery($stmt);

            $stmt = <<<EOT
CREATE TRIGGER company_fill_coordinate_trg
    BEFORE INSERT OR UPDATE OF longitude, latitude
    ON company
    FOR EACH ROW
    EXECUTE PROCEDURE company_fill_coordinate();
EOT;
            $connection->executeQuery($stmt);
        } catch (DBALException $exception) {
            throw new RuntimeException('Trigger "company_fill_coordinate_trg" cannot be created.');
        }

        return $return;
    }

}
