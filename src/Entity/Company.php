<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Swagger\Annotations as SWG;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CompanyRepository")
 */
class Company
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     *
     * @SWG\Property(
     *     type="integer",
     *     description="The unique identifier of the user",
     *     example=1
     * )
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="value", type="text", length=65535, nullable=false)
     *
     * @SWG\Property(
     *     description="The data of the user",
     *     type="object",
     *     example={"companyName":"Beispiel Firma","ownerName":"Max Mustermann","industry":"baecker","zip":"030","city":"Berlin","street":"Karl-Marx-Stra\u00dfe 30","contactTimes":"Mo - Sa 08:00 - 18:00","phone":"","email":"meinunternehmen@web.de","facebook":"","web":"","description":"Lorem ipsum..."}
     * )
     */
    private $value;

    /**
     * @var float
     *
     * @ORM\Column(name="latitude", type="float", nullable=true)
     *
     * @SWG\Property(
     *     type="float",
     *     description="The latitude of the user",
     *     example=51.25703
     * )
     */
    private $latitude;

    /**
     * @var float
     *
     * @ORM\Column(name="longitude", type="float", nullable=true)
     *
     * @SWG\Property(
     *     type="float",
     *     description="The longitude of the user",
     *     example=7.16868
     * )
     */
    private $longitude;

    /**
     * @var string
     *
     * @ORM\Column(name="coordinate", type="geometry", nullable=true)
     */
    private $coordinate;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getValue(): ?string
    {
        return $this->value;
    }

    /**
     * @param string $value
     *
     * @return Company
     */
    public function setValue(string $value): Company
    {
        $this->value = $value;

        return $this;
    }

    /**
     * @return float|null
     */
    public function getLatitude(): ?float
    {
        return $this->latitude;
    }

    /**
     * @param float $latitude
     *
     * @return Company
     */
    public function setLatitude(float $latitude): Company
    {
        $this->latitude = $latitude;

        return $this;
    }

    /**
     * @return float|null
     */
    public function getLongitude(): ?float
    {
        return $this->longitude;
    }

    /**
     * @param float $longitude
     *
     * @return Company
     */
    public function setLongitude(float $longitude): Company
    {
        $this->longitude = $longitude;

        return $this;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        $company = [
            'id' => $this->getId(),
            'longitude' => $this->getLongitude(),
            'latitude' => $this->getLatitude()
        ];

        $company = array_merge($company, json_decode($this->getValue(), true));

        return $company;
    }
}
