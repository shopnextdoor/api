<?php

namespace App\Controller;

use App\Entity\Company;
use App\Repository\CompanyRepository;
use Nelmio\ApiDocBundle\Annotation\Model;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Swagger\Annotations as SWG;

/**
 * CompanyController
 * @package App\Controller
 *
 * @SWG\Tag(name="company")
 */
class CompanyController extends AbstractController
{
    /**
     * Get all companies
     *
     * @Route("/api/company/list", methods={"GET"})
     * @SWG\Response(
     *     response=200,
     *     description="Returns a list of companies",
     *     @Model(type=Company::class)
     * )
     * @SWG\Parameter(
     *     name="latitude",
     *     in="query",
     *     type="number",
     *     description="If set, a latitude must also be given"
     * )
     * @SWG\Parameter(
     *     name="longitude",
     *     in="query",
     *     type="number",
     *     description="If set, a longitude must also be given"
     * )
     * @SWG\Parameter(
     *     name="distance",
     *     in="query",
     *     type="number",
     *     default="15000",
     *     description="Number of the radius search in meters"
     * )
     *
     * @param Request $request
     *
     * @return JsonResponse
     * @throws \Exception
     */
    public function listAction(Request $request)
    {
        $result = [];
        $repository = new CompanyRepository($this->getDoctrine());

        if (($request->get('latitude') && $request->get('longitude')) &&
            (!empty($request->get('latitude')) && !empty($request->get('longitude')))
        ) {
            $result = $repository->findByGeo(
                (float) $request->get('latitude'),
                (float) $request->get('longitude'),
                !empty($request->get('distance')) ? (int) $request->get('distance') : 15000
            );
        } else {
            $result = $repository->findAll();
        }

        if (empty($result)) {
            return $this->createJsonResponse([], 404);
        }

        return $this->createJsonResponse($result);
    }

    /**
     * Creates a new company
     *
     * @Route("/api/company", methods={"POST"})
     * @SWG\Response(
     *     response=200,
     *     description="Returns the created company",
     *     @Model(type=Company::class)
     * )
     * @SWG\Parameter(
     *     name="company",
     *     in="body",
     *     type="object",
     *     @SWG\Schema(
     *         type="object",
     *         required={"value"},
     *         ref=@Model(type=Company::class)
     *     )
     * )
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function addAction(Request $request)
    {
        $data = $request->getContent();
        $manager = $this->getDoctrine()->getManager();

        if (empty($data)) {
            return $this->createJsonResponse([], 404);
        }

        if (isset($data['id'])) {
            unset($data['id']);
        }

        $company = new Company();
        $company->setValue($data);

        $manager->persist($company);
        $manager->flush();

        return $this->createJsonResponse($company->toArray());
    }

    /**
     * Get a single company
     *
     * @Route("/api/company/{id}", methods={"GET"})
     * @SWG\Response(
     *     response=200,
     *     description="Returns the requested company",
     *     @Model(type=Company::class)
     * )
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function showAction(Request $request)
    {
        $repository = new CompanyRepository($this->getDoctrine());
        $result = $repository->findOneBy([
            'id' => $request->get('id')
        ]);

        if (empty($result)) {
            return $this->createJsonResponse([], 404);
        }

        return $this->createJsonResponse($result->toArray());
    }

    /**
     * Updates an existing company
     *
     * @Route("/api/company/{id}", methods={"PUT"})
     * @SWG\Response(
     *     response=200,
     *     description="Returns the updated company",
     *     @Model(type=Company::class)
     * )
     *
     * @SWG\Parameter(
     *     name="company",
     *     in="body",
     *     type="object",
     *     schema=@SWG\Schema(
     *         type="object",
     *         required={"value"},
     *         ref=@Model(type=Company::class))
     *     )
     * )
     *
     * @param Request $request
     *
     * @return JsonResponse
     * @todo - Secure
     *
     */
    public function editAction(Request $request)
    {
        return $this->createJsonResponse(['message' => 'not implemented yet'], 404);

        // @todo - Kann erst im Zuge des Frotends implementiert werden
        $data = json_decode($request->get('company'), true);
        $manager = $this->getDoctrine()->getManager();
        $repository = new CompanyRepository($this->getDoctrine());

        $currentCompany = $repository->findOneBy([
            'id' => $request->get('id')
        ]);

        if (empty($data) || empty($currentCompany)) {
            return $this->createJsonResponse([], 404);
        }

        unset($data['id']);

        // @todo - longitude and latitude still have to be recalculated
        unset($data['longitude']);
        unset($data['latitude']);

        $currentCompany->setValue($data);

        $manager->persist($currentCompany);
        $manager->flush();

        return $this->createJsonResponse($currentCompany->toArray());
    }

    /**
     * @param array $data
     * @param int $status
     *
     * @return JsonResponse
     */
    protected function createJsonResponse(array $data, int $status = 200)
    {
        $response = new JsonResponse();

        $response->setData($data);
        $response->setStatusCode($status);
        $response->headers->set('Access-Control-Allow-Origin', '*');

        return $response;
    }
}
