<?php

namespace App\Repository;

use App\Entity\Company;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\DBAL\DBALException;
use Doctrine\ORM\Query;

/**
 * @method Company|null find($id, $lockMode = null, $lockVersion = null)
 * @method Company|null findOneBy(array $criteria, array $orderBy = null)
 * @method Company[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CompanyRepository extends ServiceEntityRepository
{
    /**
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Company::class);
    }

    /**
     * @return Company[]
     */
    public function findAll()
    {
        $result = $this->createQueryBuilder('c')
            ->orderBy('c.id', 'ASC')
            ->getQuery()
            ->getResult(Query::HYDRATE_ARRAY);

        foreach ($result as &$company) {
            $company = $this->formatData($company);
        }

        return $result;
    }

    /**
     * @param float $latitude
     * @param float $longitude
     * @param int $distanceMeters
     *
     * @return Company[]
     * @throws DBALException
     */
    public function findByGeo(float $latitude, float $longitude, int $distanceMeters): array
    {
        $statement = <<<EOT
SELECT *
FROM company
WHERE ST_Distance(ST_GeogFromText('SRID=4326;POINT('||to_char(:longitude::float8,'999.99999')||' '||to_char(:latitude::float8,'999.99999')||')'), coordinate) < :distance_m;
EOT;

        $result = $this->getEntityManager()->getConnection()->executeQuery($statement, [
            'latitude' => $latitude,
            'longitude' => $longitude,
            'distance_m' => $distanceMeters
        ])->fetchAll();

        foreach ($result as &$company) {
            $company = $this->formatData($company);
        }

        return $result;
    }

    /**
     * @param array $data
     *
     * @return array
     */
    protected function formatData(array $data): array
    {
        $company = [
            'id' => $data['id'],
            'longitude' => $data['longitude'],
            'latitude' => $data['latitude']
        ];

        $company = array_merge($company, json_decode($data['value'], true));

        return $company;
    }
}
